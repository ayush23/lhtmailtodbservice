/**
 * Created by ayush on 12/19/16.
 */

//require node modules
const Dotenv = require('dotenv').config({silent: true});
const Express = require('express');
const BodyParser = require('body-parser');
const Mongoose = require('mongoose');

//require file
const Config = require('./config');

//create app
const APP = Express();
APP.use(BodyParser.urlencoded({ extended: false }));
APP.use(BodyParser.json());
require('./config/express')(APP);
require('./routes')(APP);

const connection = connect();
connection
    .on('error', console.log)
    .on('disconnected', connect)
    .once('open', listen);

//start imap server
const MailListener = require('./app/modules/MailListener/index');
MailListener.getMail();

const PORT = process.env.PORT || 3001;
function listen() {
    APP.listen(PORT);
    console.log('Express app started on port ' + PORT);
}

function connect() {
    console.log(Config.DB);

    let options = {
        server: {socketOptions: {keepAlive: 1, connectTimeoutMS: 500000}}
    };

    return Mongoose.connect(Config.DB, options).connection;
}

module.exports = APP;


