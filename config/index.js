/**
 * Created by ayush on 12/19/16.
 */
var path = require('path');
var extend = require('util')._extend;

var local = require('./env/local');
var development = require('./env/development');
var test = require('./env/test');
var production = require('./env/production');

var defaults = {
    root: path.normalize(__dirname + '/..')
};

/**
 * Expose
 */

module.exports = {
    development: extend(development, defaults),
    test: extend(test, defaults),
    production: extend(production, defaults),
    local: extend(local, defaults)
}[process.env.NODE_ENV || 'development'];

