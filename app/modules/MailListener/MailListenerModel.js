/**
 * Created by ayush on 12/20/16.
 */

/*
 * Module dependencies
 */
var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;

/**
 * MailListener Schema
 */

var MailListenerSchema = new Schema({
    from: [{
        address: {type: String, default: ''},
        name: {type: String, default: ''},
    }],
    to: [{
        address: {type: String, default: ''},
        name: {type: String, default: ''},
    }],
    subject: {type: String, default: ''},
    html: {type: String, default: ''},
    text: {type: String, default: ''},
    metaData: {},
    addedOn: {type: Number, default: 0},
    modifiedOn: {type: Number, default: new Date().getTime()},
    isActive: {type: Number, default: 1},
    isDeleted: {type: Number, default: 0}
});

/**
 * Methods
 */
MailListenerSchema.method({
    saveMailListener: function() {
        return this.save();
    }
});

MailListenerSchema.static({
    getMailList: function (queryObj, skip, limit) {
        return this.find(queryObj).skip(Number(skip)).limit(Number(limit)).exec();
    },
    updateMail: function (mailObj) {
        return this.findOneAndUpdate({_id: mailObj._id}, mailObj, {new: true}).exec();
    },
});

Mongoose.model('MLS-MailListener', MailListenerSchema);