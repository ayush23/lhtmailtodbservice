/**
 * Created by ayush on 12/20/16.
 */

//require node modules
const co = require('co');
const Mongoose = require('mongoose');
const MailListener = require("mail-listener2");

//require files and models
const Constants = require('../../common/Constants');
const Config = require('../../../config/index');
const Utils = require('../../utils/index');
const MailListenerFile = require('./MailListenerModel.js');
const MailListenerModel = Mongoose.model('MLS-MailListener');

module.exports = {
    getMailFromMailBox: co.wrap(function *() {

        let mailListener = new MailListener({
            username: Config.EMAIL,
            password: Config.PASSWORD,
            host: Config.HOST,
            port: Config.PORT,
            tls: true,
            debug: null,
            tlsOptions: {rejectUnauthorized: false},
            markSeen: true,
            attachments: false,
            // attachmentOptions: {directory: Constants.ATTACHMENT_DIRECTORY}
        });

        mailListener.start();

        mailListener.on("server:connected", function () {
            console.log("IMAP_CONNECTED");
        });

        mailListener.on("server:disconnected", function () {
            console.log("IMAP_DISCONNECTED");
        });

        mailListener.on("error", function (err) {
            console.log(err);
        });

        mailListener.on("mail", responseFunction)
    }),

    getMailList: co.wrap(function *(req, res) {
        let queryObj = req.query;
        let skip = 0;
        let limit = 0;

        if(queryObj.skip){
            skip = queryObj.skip;
        }

        if(queryObj.limit){
            limit = queryObj.limit;
        }

        let getMailListResponse = yield MailListenerModel.getMailList(queryObj, skip, limit).catch(function (err) {
            return Utils.res(res, {}, err.message, Constants.RESPONSE.RESPONSE_FAILURE);
        });

        if (!getMailListResponse) {
            return Utils.res(res, [], Constants.MSG.GET_MAIL_LIST_FAILURE, Constants.RESPONSE.RESPONSE_FAILURE);
        }

        if (getMailListResponse.length == 0) {
            return Utils.res(res, getMailListResponse, Constants.MSG.NO_MAIL_LIST_FOUND, Constants.RESPONSE.RESPONSE_SUCCESS);
        }

        return Utils.res(res, getMailListResponse, Constants.MSG.GET_MAIL_LIST_SUCCESS, Constants.RESPONSE.RESPONSE_SUCCESS);
    }),

    updateMail: co.wrap(function *(req, res) {

        let updateMailResponse = yield MailListenerModel.updateMail(req.body).catch(function (err) {
            return Utils.res(res, {}, err.message, Constants.RESPONSE.RESPONSE_FAILURE);
        });

        if (!updateMailResponse) {
            return Utils.res(res, {}, Constants.MSG.UPDATE_MAIL_FAILURE, Constants.RESPONSE.RESPONSE_FAILURE);
        }

        return Utils.res(res, updateMailResponse, Constants.MSG.UPDATE_MAIL_SUCCESS, Constants.RESPONSE.RESPONSE_SUCCESS);
    })
};

let responseFunction = co.wrap(function *(mail, seqno, attributes) {

    let mailListenerObj = new MailListenerModel();
    mailListenerObj.from = mail.from;
    mailListenerObj.to = mail.to;
    mailListenerObj.subject = mail.subject;
    mailListenerObj.html = mail.html;
    mailListenerObj.text = mail.text;
    mailListenerObj.addedOn = new Date().getTime();

    return yield mailListenerObj.saveMailListener();
});