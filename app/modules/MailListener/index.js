/**
 * Created by ayush on 12/20/16.
 */

const co = require('co');
const MailListenerBLManager = require('./MailListenerBLManager');

exports.getMail = co.wrap(function *(req, res) {
    MailListenerBLManager.getMailFromMailBox();
});

exports.getMailList = co.wrap(function *(req, res) {
    return yield MailListenerBLManager.getMailList(req, res);
});

exports.updateMail = co.wrap(function *(req, res) {
    return yield MailListenerBLManager.updateMail(req, res);
});