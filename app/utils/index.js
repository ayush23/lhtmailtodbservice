/**
 * Created by ayush on 12/20/16.
 */

module.exports = {
    res: response
};


function response(res, data, message, success, code = 200) {
    let messageObj = {
        "param": "",
        "msg": message,
        "responseCode": code
    };

    let responseObj = {
        responseData: data,
        message: [messageObj],
        success: success,
        responseCode:code
    };

    res.format({
        json: () => {
            res.send(responseObj);
        }
    });
}
