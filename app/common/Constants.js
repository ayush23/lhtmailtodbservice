/**
 * Created by ayush on 12/20/16.
 */
module.exports = {
    ATTACHMENT_DIRECTORY: "./attachments/",

    RESPONSE: {
        RESPONSE_SUCCESS: true,
        RESPONSE_FAILURE: false
    },

    MSG: {
        GET_MAIL_LIST_FAILURE: "Unable to fetch mail list",
        NO_MAIL_LIST_FOUND: "No mail found",
        GET_MAIL_LIST_SUCCESS: "Mail list fetched successfully",
        UPDATE_MAIL_FAILURE: "Unable to update mail",
        UPDATE_MAIL_SUCCESS: "Mail updated successfully"
    }
};