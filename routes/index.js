/**
 * Created by ayush on 12/19/16.
 */
const MailListener = require('../app/modules/MailListener/index');

module.exports = function (app) {
    app.get('/', function (req, res) {
        res.send("Its working fine");
    });

    /**
     * get-mail-list api
     */
    app.get('/get-mail-list', MailListener.getMailList);

    /**
     * update-mail api
     */
    app.post('/update-mail', MailListener.updateMail);
};